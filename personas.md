---
layout: page
title: All Characters
permalink: /characters/
---

Teams have been working together to create characters with their own backgrounds, hobbies, and worries about starting university, and finding ways that they can get involved with the Monash community.

<div class="personas_header_container">
  <div class="divider"></div>
  <div class="personas_header">
    <b>Created Characters</b>
  </div>
  <div class="divider"></div>
</div>

<div class="post_list">
    {% assign sortedPosts = site.posts | sort: 'order' %}
    {% for post in sortedPosts %}
      <div class="post_card">
        <a href="{{ post.url }}">
          {% if post.profile_pic %} 
              <img class="profile_pic" src="{{post.profile_pic}}"/>
          {% endif %}
          <h3>{{post.name}}</h3>
          <p class="post-meta">{{post.group}}</p>
        </a>
      </div>
    {% endfor %}
  </div>