---
layout: page
title: About
permalink: /about/
---

**Running from 22 Feb to 1 March, IdeaFest is a social event designed to connect new students to each other and their peers in the IT student community.**

We know starting university can be  daunting&mdash;but don't worry, we're here to help! IdeaFest will help you to get your studies at Monash University off to a great start: helping you to become more familiar with your course and key staff, the support services available, as well as the activities that will make your time here all the more memorable.

Using the messaging app of your choice, IdeaFest will connect you to other new students, in both randomised team group chats and larger groups based on your interests. In your teams, you'll work together to complete activities: each designed to help you quickly become a part of our active IT community by meeting new friends and connecting to clubs and societies of interest. Each group chat will also have an experienced student mentor, who will be able to answer any questions you have about life at Monash or the event itself.

Your ideas and discussions generated during IdeaFest will also be used to help shape the experiences of future commencing student groups.

<p style="text-align:center;margin-top:25px">Register here to get started!</p>
<button onclick="location.href='https://docs.google.com/forms/d/e/1FAIpQLSfzR_d3q41zlVsm3-H3U1CvJpgZCfHLgWuvc3tDusryqTZYQQ/viewform?usp=pp_url&entry.128062499=Continue+to+know+more+information.&entry.2082725650=I+have+read+and+understand+the+above+statement,+and+consent+to+this+research.'" type="button">Register</button>
