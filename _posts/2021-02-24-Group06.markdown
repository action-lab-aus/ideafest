---
layout: persona
group: "Group 6"
name:  "Shin"
order: "06"
profile_pic: "/images/personas/Group6/profilepic.jpg"
---

### Bio
![A robot, not sure honestly](/images/personas/Group6/coding.jpg)
Shin is joining Monash University as a student of computer science. He really into computer game and programming. His parents encouraged him to pursue his dream of becoming programmer. Shin is more like introvert so he don’t socialize much with other students. He always spend his time playing his games and learning to code. During his time at Monash he hopes to develop his skills and start his career as Computer Scientist. 

### Interests
![Gaming](/images/personas/Group6/games.jpg)
Shin’s hobbies are playing video games and learn his programming skills. He’s been playing games since 2014. Shin usually make friends through online gaming such as Dota2. During his time at Monash, Shin starts reducing his gaming time and learning to code more. He also joined MAC (Monash Association of Coding) to improve his skills on programming as well.

### Worries
Shin is more like an introvert as he rarely talk to other people outside. However, he decided to register Idea Fest in Monash which is a social event to connect every student in school. He chose gaming and programming group based on his interests then group a team to work on a challenge together. Moreover, Shin also struggle with his units selection. He doesn’t know what to study in his very first year in university. Nevertheless, he contacted Monash career advisor and student services to help with his unit selection. As a result, Shin got lots of help from Monash to cope with his socializing and units selection successfully.