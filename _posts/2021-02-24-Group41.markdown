---
layout: persona
group: "Group 41"
name:  "Pyth0n41"
order: "41"
profile_pic: "/images/personas/Group41/profilepic.png"
---

### Bio
![Pyth0n41 character](/images/personas/Group41/character.png)
Pyth0n41 is a robotics project, belonging to Monash University. A Monash professor discovered an AI consciousness named Pyth0n41 trapped in the project, and made a deal with it: together they would study how Pyth0n41 came to be an AI, and also give it the opportunity to make friends.

### Interests

Pyth0n41 likes video games - its favourite game is Snake. To meet new friends to play with, it will join MEGA (the Monash Electronic Gaming Association). Pyth0n41 also enjoys anime and reading - it thinks those are great ways to experience human culture and, importantly, make friends! 